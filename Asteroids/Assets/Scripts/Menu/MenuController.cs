using Shared;
using UnityEngine;
using Shared.Events;
using Shared.DependencyInjection;
using Menu.Event;

namespace Menu
{
	public class MenuController : MonoBehaviour
	{

		[Inject (Context.Menu)]
		private EventDispatcher eventDispatcher;


		private void Awake ()
		{
			this.Inject ();
		}

		public void OnPlayButtonClick ()
		{
			eventDispatcher.Dispatch<LaunchGameEvent> (new LaunchGameEvent ());
		}

		public void OnQuitButtonClick ()
		{
			Application.Quit ();
		}
	}
}
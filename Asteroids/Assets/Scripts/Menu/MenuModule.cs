using Shared;
using Shared.DependencyInjection;
using Shared.Events;

namespace Menu
{
	public class MenuModule: IModule
	{
		private InjectProvider injectProvider;

		public MenuModule ()
		{
		}

		#region IModule implementation

		public void Initialise ()
		{
			InjectProvider.CreateProvider (Context.Menu);
			injectProvider = InjectProvider.GetProvider (Context.Menu);
			injectProvider.Map<EventDispatcher> ();
		}

		public void Dispose ()
		{
			InjectProvider.DestroyProvider (Context.Menu);
		}

		public string GetSceneName ()
		{
			return "menu";
		}

		#endregion
	}
}
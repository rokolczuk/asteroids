using Game.View.GameObjects;
﻿
using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof(AsteroidMeshGenerator))]
public class DynamicPolygonMeshEditor : Editor
{

	override public void  OnInspectorGUI ()
	{

		DrawDefaultInspector ();

		AsteroidMeshGenerator colliderCreator = (AsteroidMeshGenerator)target;

		if (GUILayout.Button ("Generate"))
		{
			colliderCreator.Generate (); 
		}
	}
}

using UnityEngine;
using Game.Model;
using Shared.DependencyInjection;
using Shared;

namespace Game.View.Util
{

	[RequireComponent (typeof(Camera))]
	public class GameBoundsUpdater : MonoBehaviour
	{

		[Inject (Context.Game)]
		private GameModel gameModel;

		private Camera gameCamera;

		private int screenWidth = 0;
		private int screenHeight = 0;

		private void Awake ()
		{
			gameCamera = GetComponent<Camera> ();
			this.Inject ();
			UpdateBounds ();
		}

		private void Update ()
		{
			if (screenWidth != Screen.width || screenHeight != Screen.height)
			{
				UpdateBounds ();
			}
		}

		private void UpdateBounds ()
		{
			float screenAspect = (float)Screen.width / (float)Screen.height;
			float cameraHeight = gameCamera.orthographicSize * 2;

			Vector3 cameraCenter = gameCamera.transform.position;
			cameraCenter.z = 0;

			gameModel.CameraBounds = new Bounds (
				cameraCenter,
				new Vector3 (cameraHeight * screenAspect, cameraHeight, 0)
			);
		}
	}

}
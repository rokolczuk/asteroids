using UnityEngine;
using Game.Model;
using Shared.DependencyInjection;
using Shared;

namespace Game.View.Util
{
	public class CameraBoundsConstraint: MonoBehaviour
	{
		[Inject (Context.Game)]
		private GameModel gameModel;

		private void Awake ()
		{
			this.Inject ();
		}

		private void Update ()
		{
			Vector3 position = transform.position;

			if (!gameModel.CameraBounds.Contains (position))
			{

				if (position.x < gameModel.CameraBounds.min.x)
				{
					position.x += gameModel.CameraBounds.size.x;
				}

				if (position.x > gameModel.CameraBounds.max.x)
				{
					position.x -= gameModel.CameraBounds.size.x;
				}

				if (position.y < gameModel.CameraBounds.min.y)
				{
					position.y += gameModel.CameraBounds.size.y;
				}

				if (position.y > gameModel.CameraBounds.max.y)
				{
					position.y -= gameModel.CameraBounds.size.y;
				}

				transform.position = position;
			}
		}
	}
}
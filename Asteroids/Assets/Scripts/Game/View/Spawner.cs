using UnityEngine;
using Shared.Events;
using Game.Model;
using Game.Model.Scoring;
using Shared.Pooling;
using Game.View.GameObjects.Ship;
using Shared.DependencyInjection;
using Game.Event;
using Game.View.GameObjects;
using System.Collections.ObjectModel;
using System;
using Shared;

namespace Game.View
{

	public class Spawner : MonoBehaviour
	{
		[Inject (Context.Game)]
		private EventDispatcher eventDispatcher;

		[Inject (Context.Game)]
		private GameModel gameModel;

		[Inject (Context.Game)]
		private ScoringConfiguration scoringConfiguration;

		[SerializeField]
		private Pool bulletsPool;

		[SerializeField]
		private Pool asteroidsPool;

		[SerializeField]
		private Pool enemiesPool;

		[SerializeField]
		private Pool playerPool;

		[SerializeField]
		private Pool collectiblesPool;

		private Spaceship playerShip;

		private void Awake ()
		{
			this.Inject ();

			eventDispatcher.AddEventListener<SpawnBulletEvent> (OnSpawnBullet);
			eventDispatcher.AddEventListener<SpawnAsteroidEvent> (OnSpawnAsteroid);
			eventDispatcher.AddEventListener<AsteroidDestroyedEvent> (OnAsteroidDestroyed);
			eventDispatcher.AddEventListener<SpawnEnemyEvent> (OnSpawnEnemy);
			eventDispatcher.AddEventListener<SpawnPowerupEvent> (OnSpawnPowerup);
			eventDispatcher.AddEventListener<SpawnPlayerEvent> (OnSpawnPlayer);
			eventDispatcher.AddEventListener<RemoveAllGameObjectsEvent> (OnRemoveAllGameObjectsRequest);

			this.Inject ();
		}

		private void OnSpawnPlayer (SpawnPlayerEvent e)
		{
			playerShip = playerPool.Get<Spaceship> ();
			playerShip.transform.position = new Vector3 (0, -2, 0);
			playerShip.transform.rotation = Quaternion.Euler (Vector3.up);
			gameModel.AddShip (playerShip, true);
		}

		private void OnSpawnBullet (SpawnBulletEvent e)
		{
			Bullet bullet = bulletsPool.Get<Bullet> ();

			bullet.transform.position = e.Position;
			bullet.Shoot (e.Angle, e.CollideWith, e.Damage);
		}

		private void OnSpawnAsteroid (SpawnAsteroidEvent spawnAsteroidEvent)
		{
			Asteroid asteroid = asteroidsPool.Get<Asteroid> ();
			asteroid.Initialise (spawnAsteroidEvent.Size, spawnAsteroidEvent.Position, spawnAsteroidEvent.Angle);

			gameModel.AddAsteroid (asteroid);
		}

		private void OnAsteroidDestroyed (AsteroidDestroyedEvent asteroidDestroyedEvent)
		{
			gameModel.AddScore (scoringConfiguration.GetScoreForAsteroidSize (asteroidDestroyedEvent.Asteroid.Size));
			gameModel.RemoveAsteroid (asteroidDestroyedEvent.Asteroid);
		}

		private void OnSpawnEnemy (SpawnEnemyEvent spawnEnemyEvent)
		{
			Spaceship enemyShip = enemiesPool.Get<Spaceship> ();
			enemyShip.transform.position = spawnEnemyEvent.Position;
			gameModel.AddShip (enemyShip, false);
		}

		private void OnSpawnPowerup (SpawnPowerupEvent spawnPowerupEvent)
		{
			Collectible collectible = collectiblesPool.Get<Collectible> ();
			collectible.SetPowerup (spawnPowerupEvent.PowerupData);
			collectible.transform.position = spawnPowerupEvent.Position;
			gameModel.AddCollectible (collectible);
		}
			
		private void OnRemoveAllGameObjectsRequest (RemoveAllGameObjectsEvent restartGameEvent)
		{
			if (gameModel.PlayerShip)
			{
				playerPool.Return (gameModel.PlayerShip.gameObject);
				gameModel.RemoveShip (gameModel.PlayerShip);
			}

			ReadOnlyCollection<Asteroid> asteroids = gameModel.Asteroids;
			ReturnToPool<Asteroid> (asteroids, asteroidsPool, gameModel.RemoveAsteroid);

			ReadOnlyCollection<Collectible> collectibles = gameModel.Collectibles;
			ReturnToPool<Collectible> (collectibles, collectiblesPool, gameModel.RemoveCollectible);

			ReadOnlyCollection<Spaceship> ships = gameModel.Ships;
			ReturnToPool<Spaceship> (ships, enemiesPool, gameModel.RemoveShip);
		}

		private void ReturnToPool<T> (ReadOnlyCollection<T> instances, Pool objectPool, Action<T> removeFromModelMethod) where T: MonoBehaviour
		{
			while (instances.Count > 0)
			{
				objectPool.Return (instances [0].gameObject);
				removeFromModelMethod (instances [0]);
			}
		}
			
		private void OnDestroy ()
		{
			eventDispatcher.RemoveEventListener<SpawnBulletEvent> (OnSpawnBullet);
			eventDispatcher.RemoveEventListener<SpawnAsteroidEvent> (OnSpawnAsteroid);
			eventDispatcher.RemoveEventListener<AsteroidDestroyedEvent> (OnAsteroidDestroyed);
			eventDispatcher.RemoveEventListener<SpawnEnemyEvent> (OnSpawnEnemy);
			eventDispatcher.RemoveEventListener<SpawnPowerupEvent> (OnSpawnPowerup);
			eventDispatcher.RemoveEventListener<SpawnPlayerEvent> (OnSpawnPlayer);
			eventDispatcher.RemoveEventListener<RemoveAllGameObjectsEvent> (OnRemoveAllGameObjectsRequest);
		}
	}
}
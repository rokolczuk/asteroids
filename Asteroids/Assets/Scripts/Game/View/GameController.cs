using UnityEngine;
using Shared.Events;
using Game.Model.Levels;
using Game.Model;
using Game.Model.Powerups;
using Shared.DependencyInjection;
using Game.Event;
using Game.Event.UI;
using Shared;
using System.Collections;

namespace Game.View
{

	public class GameController : MonoBehaviour
	{
		[Inject (Context.Game)]
		private EventDispatcher eventDispatcher;

		[Inject (Context.Game)]
		private Levels levels;

		[Inject (Context.Game)]
		private GameModel gameModel;

		[Inject (Context.Game)]
		private Powerups powerups;

		[SerializeField]
		[Range (0, 4)]
		private int startOnLevel = 0;

		private float timeEnemySpawned;
		private float timePowerupSpawned;

		private bool levelInProgress;

		private LevelData currentLevel;

		private void Awake ()
		{
			this.Inject ();

			gameModel.Reset ();

			for (int i = 0; i < startOnLevel; i++)
			{
				gameModel.CompleteLevel ();
			}
			
			eventDispatcher.AddEventListener<StartLevelEvent> (OnStartLevel);
			eventDispatcher.AddEventListener<RestartGameEvent> (OnRestartGame);
		}

		private void Start ()
		{
			eventDispatcher.Dispatch<SpawnPlayerEvent> (new SpawnPlayerEvent ());
			eventDispatcher.Dispatch<ShowPreLevelSwipeEvent> (new ShowPreLevelSwipeEvent (gameModel.CurrentLevel + 1, levels.GetLevel(gameModel.CurrentLevel).Tutorial));
		}

		private void Update ()
		{
			if (!levelInProgress)
			{
				return;
			}

			if (gameModel.GameOver)
			{
				levelInProgress = false;
				eventDispatcher.Dispatch<GameOverEvent> (new GameOverEvent ());
			}

			if (currentLevel.EnemyConfiguration.SpawnEnemiesInInterval && Time.time >= timeEnemySpawned + currentLevel.EnemyConfiguration.EnemySpawnInterval)
			{
				SpawnEnemy ();
			}

			if (currentLevel.HasPowerups && Time.time >= timePowerupSpawned + currentLevel.PowerupSpawnIntervalTime)
			{
				SpawnPowerup ();
			}

			if (gameModel.Asteroids.Count == 0 && gameModel.Ships.Count == 1)
			{
				CompleteLevel ();
			}
		}

		private void OnStartLevel (StartLevelEvent e)
		{
			levelInProgress = true;

			currentLevel = levels.GetLevel (gameModel.CurrentLevel);

			timeEnemySpawned = Time.time;
			timePowerupSpawned = Time.time -currentLevel.PowerupSpawnIntervalTime;

			for (int i = 0; i < currentLevel.asteroidSizes.Count; i++)
			{
				Vector3 position = gameModel.GetRandomAvailablePosition ();
				float randomAngle = Random.Range (0f, 360f);

				eventDispatcher.Dispatch<SpawnAsteroidEvent> (new SpawnAsteroidEvent (position, randomAngle, currentLevel.asteroidSizes [i]));
			}

			if(currentLevel.EnemyConfiguration.SpawnEnemiesAtStart && currentLevel.EnemyConfiguration.NumEnemiesAtStart > 0)
			{
				StartCoroutine(SpawnInitialEnemies(currentLevel.EnemyConfiguration.FirstEnemyWaveDelay, currentLevel.EnemyConfiguration.NumEnemiesAtStart));
			}
		}

		private IEnumerator SpawnInitialEnemies (float delay, int numEnemies)
		{
			yield return new WaitForSeconds(delay);

			for (int i = 0; i < numEnemies; i++)
			{
				SpawnEnemy ();
			}
		}

		private void CompleteLevel ()
		{
			levelInProgress = false;
			gameModel.CompleteLevel ();

			if (gameModel.CurrentLevel < levels.NumLevels)
			{
				eventDispatcher.Dispatch<ShowPreLevelSwipeEvent> (new ShowPreLevelSwipeEvent (gameModel.CurrentLevel + 1, levels.GetLevel(gameModel.CurrentLevel).Tutorial));
			} 
			else
			{
				eventDispatcher.Dispatch<GameOverEvent> (new GameOverEvent ());
			}
		}

		private void SpawnEnemy ()
		{
			timeEnemySpawned = Time.time;
			eventDispatcher.Dispatch<SpawnEnemyEvent> (new SpawnEnemyEvent (gameModel.GetRandomAvailablePosition ()));
		}

		private void SpawnPowerup ()
		{
			timePowerupSpawned = Time.time;
			PowerupData powerup = powerups.GetRandomPowerup ();
			eventDispatcher.Dispatch<SpawnPowerupEvent> (new SpawnPowerupEvent (powerup, gameModel.GetRandomAvailablePosition (3f)));
		}

		private void OnRestartGame (RestartGameEvent restartGameEvent)
		{
			StopAllCoroutines();
			eventDispatcher.Dispatch<RemoveAllGameObjectsEvent> (new RemoveAllGameObjectsEvent ());
			gameModel.Reset ();
			eventDispatcher.Dispatch<SpawnPlayerEvent> (new SpawnPlayerEvent ());
			eventDispatcher.Dispatch<ShowPreLevelSwipeEvent> (new ShowPreLevelSwipeEvent (gameModel.CurrentLevel + 1, levels.GetLevel(gameModel.CurrentLevel).Tutorial));
		}

		private void OnDestroy ()
		{
			if (eventDispatcher != null)
			{
				eventDispatcher.RemoveEventListener<StartLevelEvent> (OnStartLevel);
				eventDispatcher.RemoveEventListener<RestartGameEvent> (OnRestartGame);
			}
		}
	}

}
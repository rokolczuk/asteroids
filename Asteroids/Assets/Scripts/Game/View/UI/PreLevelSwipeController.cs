using Shared.DependencyInjection;
using UnityEngine;
using Shared.Events;
using UnityEngine.UI;
using Game.Event.UI;
using System.Collections;
using Game.Event;
using Shared;

namespace Game.View.UI
{
	public class PreLevelSwipeController : MonoBehaviour
	{
		[Inject (Context.Game)]
		private EventDispatcher eventDispatcher;

		[SerializeField]
		private Text levelText;

		[SerializeField]
		[Range (0f, 1f)]
		private float alphaTransitionLerpStep;

		[SerializeField]
		private float showLevelLabelTime;

		private Color textColor = Color.white;

		private void Awake ()
		{
			this.Inject ();
			eventDispatcher.AddEventListener<ShowPreLevelSwipeEvent> (OnShowPreLevelSwipe);
			SetTextAlpha (0f);
			gameObject.SetActive (false);
		}

		private void OnDestroy ()
		{
			eventDispatcher.RemoveEventListener<ShowPreLevelSwipeEvent> (OnShowPreLevelSwipe);
		}

		private void OnShowPreLevelSwipe (ShowPreLevelSwipeEvent e)
		{
			gameObject.SetActive (true);
			levelText.text = "Level " + e.Level + "\n" + e.Tutorial;

			StartCoroutine (ShowSwipe ());
		}

		private IEnumerator ShowSwipe ()
		{
			SetTextAlpha (0f);

			while (textColor.a < 1f)
			{
				SetTextAlpha (textColor.a + alphaTransitionLerpStep * Time.deltaTime);
				yield return new WaitForEndOfFrame ();
			}

			yield return new WaitForSeconds (showLevelLabelTime);

			while (textColor.a > 0f)
			{
				SetTextAlpha (textColor.a - alphaTransitionLerpStep * Time.deltaTime);
				yield return new WaitForEndOfFrame ();
			}

			eventDispatcher.Dispatch<StartLevelEvent> (new StartLevelEvent ());

			gameObject.SetActive (false);
		}

		private void SetTextAlpha (float alpha)
		{
			textColor.a = Mathf.Clamp01 (alpha);
			levelText.color = textColor;
		}
	}
}
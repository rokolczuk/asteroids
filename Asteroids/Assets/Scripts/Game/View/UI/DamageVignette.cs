using UnityEngine;
using Shared.DependencyInjection;
using Shared.Events;
using UnityEngine.UI;
using Game.Event.UI;
using System.Collections;
using Shared;

namespace Game.View.UI
{

	public class DamageVignette : MonoBehaviour
	{
		[Inject (Context.Game)]
		private EventDispatcher eventDispatcher;

		[SerializeField]
		private Image vignette;

		[SerializeField]
		private float damageTakenToAlphaMultiplier;

		[SerializeField]
		private float fadeOutSpeed;

		[SerializeField]
		private float minAlpha;

		private void Awake ()
		{	
			this.Inject ();
			eventDispatcher.AddEventListener<PlayerHealthUpdatedEvent> (OnPlayerHealthUpdated);
			gameObject.SetActive (false);
		}

		private void OnPlayerHealthUpdated (PlayerHealthUpdatedEvent e)
		{
			if (e.HealthChange < 0)
			{
				gameObject.SetActive (true);
				Color color = vignette.color;
				color.a = Mathf.Max (minAlpha, -e.HealthChange * damageTakenToAlphaMultiplier);
				vignette.color = color;

				StopCoroutine (FadeOut ());
				StartCoroutine (FadeOut ());
			}
		}

		private IEnumerator FadeOut ()
		{
			while (vignette.color.a > 0)
			{
				Color color = vignette.color;
				color.a -= fadeOutSpeed * Time.deltaTime;
				vignette.color = color;
				yield return new WaitForEndOfFrame ();
			}

			gameObject.SetActive (false);
		}

		private void OnDestroy ()
		{	
			eventDispatcher.RemoveEventListener<PlayerHealthUpdatedEvent> (OnPlayerHealthUpdated);
		}
	}
}
using UnityEngine;
using Shared.Events;
using UnityEngine.UI;
using Game.Event.UI;
using Shared.DependencyInjection;
using Shared;

namespace Game.View.UI
{
	public class HudController : MonoBehaviour
	{

		[Inject (Context.Game)]
		private EventDispatcher eventDispatcher;

		[SerializeField]
		private Text scoreText;

		[SerializeField]
		private Text healthText;

		[SerializeField]
		private Text weaponText;

		[SerializeField]
		private Text ammoText;

		private void Awake ()
		{
			this.Inject ();

			eventDispatcher.AddEventListener<ScoreUpdatedEvent> (OnScoreUpdated);
			eventDispatcher.AddEventListener<PlayerHealthUpdatedEvent> (OnPlayerHealthUpdated);
			eventDispatcher.AddEventListener<AmmoUpdatedEvent> (OnAmmoUpdated);
			eventDispatcher.AddEventListener<WeaponUpdatedEvent> (OnWeaponUpdated);
		}

		private void OnScoreUpdated (ScoreUpdatedEvent e)
		{
			scoreText.text = "SCORE: " + e.Score;
		}

		private void OnPlayerHealthUpdated (PlayerHealthUpdatedEvent e)
		{
			healthText.text = "HEALTH: " + e.Health;
		}

		private void OnAmmoUpdated (AmmoUpdatedEvent e)
		{
			ammoText.text = "AMMO: " + ((e.Ammo > -1) ? e.Ammo.ToString () : "oo");
		}

		private void OnWeaponUpdated (WeaponUpdatedEvent e)
		{
			weaponText.text = "WEAPON: " + e.WeaponName;
		}

		private void OnDestroy ()
		{
			eventDispatcher.RemoveEventListener<ScoreUpdatedEvent> (OnScoreUpdated);
			eventDispatcher.RemoveEventListener<PlayerHealthUpdatedEvent> (OnPlayerHealthUpdated);
			eventDispatcher.RemoveEventListener<AmmoUpdatedEvent> (OnAmmoUpdated);
			eventDispatcher.RemoveEventListener<WeaponUpdatedEvent> (OnWeaponUpdated);
		}
	}

}
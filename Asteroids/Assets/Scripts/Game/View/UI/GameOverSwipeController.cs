using UnityEngine;
using Shared.Events;
using Shared.DependencyInjection;
using Game.Event;
using Shared;

namespace Game.View.UI
{

	public class GameOverSwipeController : MonoBehaviour
	{
		[Inject (Context.Game)]
		private EventDispatcher eventDispatcher;

		private void Awake ()
		{
			this.Inject ();
			eventDispatcher.AddEventListener<GameOverEvent> (OnShowGameOver);
			gameObject.SetActive (false);
		}

		private void OnShowGameOver (GameOverEvent e)
		{
			gameObject.SetActive (true);
		}

		public void OnRestartButtonClicked ()
		{
			eventDispatcher.Dispatch<RestartGameEvent> (new RestartGameEvent ());
			gameObject.SetActive (false);
		}

		public void OnMenuButtonClicked ()
		{
			eventDispatcher.Dispatch<LaunchMenuEvent> (new LaunchMenuEvent ());
			gameObject.SetActive (false);
		}

		private void OnDestroy ()
		{
			eventDispatcher.RemoveEventListener<GameOverEvent> (OnShowGameOver);
		}
	}

}
using UnityEngine;
using Shared.Pooling;
using Game.Model;
using System.Collections.ObjectModel;
using Shared.DependencyInjection;
using Shared;
using System.Collections;

namespace Game.View.GameObjects
{
	[RequireComponent (typeof(Rigidbody2D))]
	public class Bullet : MonoBehaviour, ISpawnedByPool
	{
		[SerializeField]
		private float speed;

		[SerializeField]
		private GameObject explosion;

		[SerializeField]
		private ParticleSystem trailParticles;

		[SerializeField]
		private SpriteRenderer spriteRenderer;

		[Inject (Context.Game)]
		private GameModel gameModel;

		private Rigidbody2D bulletRigidbody;
		private Collider2D bulletCollider;

		private ReadOnlyCollection<CollisionGroup> collideWith;
		private int damage;

		private float defaultRate;

		#region ISpawnedByPool implementation

		private Pool pool;

		public Pool Pool
		{
			get
			{
				return pool;
			}
			set
			{
				pool = value;
			}
		}

		public void Reset ()
		{
			bulletCollider.enabled = true;
			spriteRenderer.enabled = true;
			explosion.transform.SetParent(transform);
			explosion.transform.position = Vector3.zero;
			explosion.SetActive(false);
			SetTrailEmissionRate(defaultRate);

		}

		#endregion

		private void SetTrailEmissionRate(float rate)
		{
			var emissionModule = trailParticles.emission;
			var emissionRate = emissionModule.rate;

			emissionRate.constant = rate;
			emissionModule.rate = emissionRate;
		}

		private void Awake ()
		{
			bulletCollider  = GetComponent<Collider2D> ();
			bulletRigidbody = GetComponent<Rigidbody2D> ();
			this.Inject ();
			explosion.SetActive(false);
			defaultRate = trailParticles.emission.rate.constant;
		}

		public void Shoot (float angle, ReadOnlyCollection<CollisionGroup> collideWith, int damage)
		{
			this.collideWith = collideWith;
			this.damage = damage;

			transform.rotation = Quaternion.Euler (0f, 0f, angle);
			bulletRigidbody.AddForce (transform.rotation * Vector2.up * speed, ForceMode2D.Impulse);
		}

		private void OnTriggerEnter2D (Collider2D other)
		{
			IDestructable destructable = other.GetComponent<IDestructable> ();

			if (destructable != null && collideWith.Contains (destructable.CollisionGroup))
			{
				Vector2 hitDirection = bulletRigidbody.velocity;
				hitDirection.Normalize ();
				destructable.OnBulletHit (transform.position, hitDirection, damage);
				StartCoroutine(ExplodeAndReturnToPool());
			}
		}

		private IEnumerator ExplodeAndReturnToPool ()
		{
			bulletCollider.enabled = false;
			SetTrailEmissionRate(0);
			spriteRenderer.enabled = false;
			explosion.SetActive(true);
			explosion.transform.SetParent(transform.parent);
			explosion.transform.position = transform.position;
			yield return new WaitForSeconds(0.4f);

			Pool.Return (gameObject);
		}

		private void Update ()
		{
			if (!gameModel.CameraBounds.Contains (transform.position))
			{
				Pool.Return (gameObject);
			}
		}
	}

}
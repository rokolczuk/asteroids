using Game.Model;
using UnityEngine;

namespace Game.View.GameObjects
{
	public interface IDestructable
	{
		CollisionGroup CollisionGroup
		{
			get;
		}

		void OnBulletHit (Vector3 bulletPosition, Vector2 hitDirection, int damage);
	}
}
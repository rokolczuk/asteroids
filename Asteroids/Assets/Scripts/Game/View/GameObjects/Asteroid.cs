using UnityEngine;
using Shared.Pooling;
using Shared.Events;
using Shared.DependencyInjection;
using Game.Event;
using Game.Model;
using Shared;

namespace Game.View.GameObjects
{
	[RequireComponent (typeof(AsteroidMeshGenerator))]
	[RequireComponent (typeof(Rigidbody2D))]
	public class Asteroid: Spawnable, IDestructable, ISpawnedByPool
	{
		[Inject (Context.Game)]
		private EventDispatcher EventDispatcher;

		[Range (0.1f, 0.4f)]
		[SerializeField]
		private float smallestAsteroidRadius = 0.2f;

		[Range (0.1f, 0.4f)]
		[SerializeField]
		private float radiusToSizeMultiplier = 0.2f;

		[Range (0.1f, 0.4f)]
		[SerializeField]
		private float radiusRandomiserToSizeMultiplier = 0.2f;

		[Range (0.1f, 10f)]
		[SerializeField]
		private float forceToSizeDivider;

		[Range (0f, 45f)]
		[SerializeField]
		private float newAsteroidsAngleOffset;

		[Range (0.1f, 10f)]
		[SerializeField]
		private float bulletSpeedToAsteroidSpeedMultiplier;

		[Range (0.5f, 2f)]
		[SerializeField]
		private float blockSpawnRadiusMultiplier;

		public int Size { get; private set; }


		private AsteroidMeshGenerator meshGenerator;
		private Rigidbody2D rigidBody;

		private void Awake ()
		{
			this.Inject ();
			rigidBody = GetComponent<Rigidbody2D> ();
			meshGenerator = GetComponent<AsteroidMeshGenerator> ();
		}

		public void Initialise (int size, Vector3 position, float angle)
		{
			this.Size = size;
			this.BlockSpawningInRadius = smallestAsteroidRadius + size * radiusToSizeMultiplier * blockSpawnRadiusMultiplier;

			meshGenerator.Radius = smallestAsteroidRadius + size * radiusToSizeMultiplier;
			meshGenerator.RadiusRandomizer = size * radiusRandomiserToSizeMultiplier;

			meshGenerator.Generate ();

			transform.position = position;

			Quaternion direction = Quaternion.Euler (0f, 0f, angle);
			Vector2 force = direction * Vector2.up * size;
			force /= forceToSizeDivider;

			rigidBody.AddForce (force, ForceMode2D.Impulse);
		}

		#region IDestructable implementation

		public void OnBulletHit (Vector3 bulletPosition, Vector2 hitDirection, int damage)
		{
			if (Size > 1)
			{
				Vector3 basePosition = transform.position;
				Vector2 newAsteroidDirection = rigidBody.velocity + hitDirection * bulletSpeedToAsteroidSpeedMultiplier;

				float newAsteroidDirectionInAngles = Mathf.Atan2 (newAsteroidDirection.y, newAsteroidDirection.x) * Mathf.Rad2Deg - 90f;

				float firstAsteroidAngle = newAsteroidDirectionInAngles - newAsteroidsAngleOffset;
				float secondAsteroidAngle = newAsteroidDirectionInAngles + newAsteroidsAngleOffset;

				Vector3 firstAsteroidPosition = basePosition + Quaternion.Euler (0f, 0f, firstAsteroidAngle) * Vector3.up * (smallestAsteroidRadius + Size * radiusToSizeMultiplier);
				Vector3 secondAsteroidPosition = basePosition + Quaternion.Euler (0f, 0f, secondAsteroidAngle) * Vector3.up * (smallestAsteroidRadius + Size * radiusToSizeMultiplier);

				EventDispatcher.Dispatch<SpawnAsteroidEvent> (new SpawnAsteroidEvent (firstAsteroidPosition, firstAsteroidAngle, Size - 1));
				EventDispatcher.Dispatch<SpawnAsteroidEvent> (new SpawnAsteroidEvent (secondAsteroidPosition, secondAsteroidAngle, Size - 1));
			}

			EventDispatcher.Dispatch<AsteroidDestroyedEvent> (new AsteroidDestroyedEvent (this));

			Pool.Return (gameObject);
		}

		public CollisionGroup CollisionGroup
		{
			get
			{
				return CollisionGroup.Asteroids;
			}
		}

		#endregion
	}
}
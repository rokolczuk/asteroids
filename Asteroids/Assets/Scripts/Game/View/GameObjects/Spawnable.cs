using UnityEngine;
using Shared.Pooling;

namespace Game.View.GameObjects
{

	public class Spawnable : MonoBehaviour, ISpawnedByPool
	{
		public float BlockSpawningInRadius;

		#region ISpawnedByPool implementation

		private Pool pool;

		public Pool Pool
		{
			get
			{
				return pool;
			}
			set
			{
				pool = value;
			}
		}

		public virtual void Reset ()
		{
		}

		#endregion
	}

}
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.View.GameObjects
{
	[ExecuteInEditMode]
	[RequireComponent (typeof(MeshFilter))]
	[RequireComponent (typeof(PolygonCollider2D))]
	public class AsteroidMeshGenerator : MonoBehaviour
	{

		//min number of points defining a polygon
		[SerializeField]
		private int minNumPoints;

		//max number of points defining a polygon
		[SerializeField]
		private int maxNumPoints;

		//% of texture area used for asteroid uv mapping.
		//this is implemented to diversify their look
		[SerializeField]
		[Range (0.0f, 0.5f)]
		private float maxAsteroidUV;

		[Range (0.3f, 3f)]
		public float Radius;

		[Range (0.0f, 1f)]
		public float RadiusRandomizer;



		private PolygonCollider2D collider2d;
		private MeshFilter meshFilter;
		private Mesh mesh;

		private void Awake ()
		{
	
			Assert.IsTrue (minNumPoints < maxNumPoints);

			collider2d = GetComponent<PolygonCollider2D> ();

			meshFilter = GetComponent<MeshFilter> ();
			mesh = new Mesh ();

			meshFilter.mesh = mesh;
		}

		/**
	 *  Creating this mesh could be optimised using Delauney Triangulation
	 *  Currently algorithm creates triangles from points defining polygon + point in the middle of it
	 */
		public void Generate ()
		{
			mesh.Clear ();

			int numPoints = Random.Range (minNumPoints, maxNumPoints);

			//last point would be (0,0,0)
			Vector3[] vertices = new Vector3[numPoints + 1];
			Vector2[] uvs = new Vector2[numPoints + 1];
			Vector2[] colliderPoints = new Vector2[numPoints];

			float uvOffset = maxAsteroidUV + Random.Range (0f, 1f - 2 * maxAsteroidUV);


			for (int i = 0; i < numPoints; i++)
			{
				float angle = 360f * i / numPoints; 
				float vertexRadius = Radius + Random.Range (0f, RadiusRandomizer);
				Quaternion rotation = Quaternion.Euler (0f, 0f, angle);

				vertices [i] = rotation * Vector3.up * (vertexRadius); 
				uvs [i] = CalculateUV (rotation, vertexRadius, uvOffset);

				colliderPoints [i] = vertices [i];
			}
			//update polygon collider
			collider2d.SetPath (0, colliderPoints);

			//last point is the (0,0,0)
			vertices [vertices.Length - 1] = Vector3.zero;
			//set uv to the centre
			uvs [uvs.Length - 1] = new Vector2 (uvOffset, uvOffset);

			int[] triangles = new int[numPoints * 3];

			//connect all points on polygon edges with center of polygon
			for (int i = 0; i < numPoints; i++)
			{
				int firstTriangleIndex = i * 3;
				triangles [firstTriangleIndex] = i;
				triangles [firstTriangleIndex + 1] = i + 1;
				triangles [firstTriangleIndex + 2] = vertices.Length - 1;
			}

			//create last triangle connecting firs
			int lastTriangleIndex = (numPoints - 1) * 3;

			triangles [lastTriangleIndex] = 0;
			triangles [lastTriangleIndex + 1] = vertices.Length - 1;
			triangles [lastTriangleIndex + 2] = vertices.Length - 2;
			
			mesh.vertices = vertices;
			mesh.uv = uvs;
			mesh.triangles = triangles;
		}

		private Vector2 CalculateUV (Quaternion angle, float vertexRadius, float uvOffset)
		{
			float maxRadius = Radius + RadiusRandomizer;

			Vector2 uv = new Vector2 (uvOffset, uvOffset);
			Vector2 normalisedVertexPosition = angle * Vector2.up;
			uv += normalisedVertexPosition * (vertexRadius / maxRadius) * maxAsteroidUV;

			return uv;
		}
	}

}
using Game.Model;
using Game.Model.Powerups;
using Shared.DependencyInjection;
using System;
using UnityEngine;
using Shared;
using System.Collections;
using Game.View.GameObjects.Ship;

namespace Game.View.GameObjects
{

	public class Collectible : Spawnable
	{
		public Action<Collectible> OnCollected;

		[Inject (Context.Game)]
		private GameModel gameModel;

		[SerializeField]
		private SpriteRenderer spriteRenderer;

		[SerializeField]
		private SpriteRenderer backgroundSpriteRenderer;

		[SerializeField]
		private float timeActiveBeforeBlinking = 6f;

		[SerializeField]
		private float blinkTime = 0.5f;

		[SerializeField]
		private float blinkAlpha = 0.5f;

		[SerializeField]
		private int numBlinks = 5;

		private PowerupData powerup;

		public bool IsBeingCollected = false;
		private Color color = Color.white;

		public PowerupType PowerupType
		{
			get
			{
				return powerup.Type;
			}
		}

		private void Awake ()
		{
			this.Inject ();
		}

		public void SetPowerup (PowerupData powerup)
		{
			this.powerup = powerup;
			spriteRenderer.sprite = powerup.Sprite;
			StartCoroutine (BlinkAndDisappear ());
		}

		private void OnTriggerEnter2D (Collider2D other)
		{
			Spaceship ship = other.GetComponent<Spaceship> ();

			if (ship != null)
			{
				powerup.Apply (ship);
				Dispose ();
			}
		}

		private void Dispose ()
		{
			gameModel.RemoveCollectible (this);

			if (OnCollected != null)
			{
				OnCollected (this);
			}

			StopAllCoroutines ();
			ResetSprites ();
			Pool.Return (gameObject);
		}

		private void DimSprites ()
		{
			color.a = blinkAlpha;
			spriteRenderer.color = backgroundSpriteRenderer.color = color;
		}

		private void ResetSprites ()
		{
			color.a = 1f;
			spriteRenderer.color = backgroundSpriteRenderer.color = color;
		}

		private IEnumerator BlinkAndDisappear ()
		{
			yield return new WaitForSeconds (timeActiveBeforeBlinking);

			for (int i = 0; i < numBlinks; i++)
			{
				DimSprites ();
				yield return new WaitForSeconds (blinkTime);
				ResetSprites ();
				yield return new WaitForSeconds (blinkTime);

			}

			Dispose ();
		}
	}

}
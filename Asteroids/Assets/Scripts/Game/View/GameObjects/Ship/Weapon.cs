using UnityEngine;
using Shared.Events;
using Game.Model.Weapons;
using Shared.DependencyInjection;
using System.Collections.Generic;
using Game.Model;
using Game.Event;
using Game.Event.UI;
using Shared;

namespace Game.View.GameObjects.Ship
{
	public class Weapon : MonoBehaviour
	{
		[Inject (Context.Game)]
		private EventDispatcher eventDispatcher;

		[SerializeField]
		private WeaponData defaultWeaponData;

		[SerializeField]
		private WeaponData currentWeaponData;

		[SerializeField]
		private bool dispatchStatusEvents;

		private float lastTimeShot;
		private int bulletsLeft;

		private void Awake ()
		{
			this.Inject ();
		}

		private void Start ()
		{
			SetWeaponData (currentWeaponData != null ? currentWeaponData : defaultWeaponData);
			lastTimeShot = -currentWeaponData.CooldownTime;
		}

		public void Shoot (List<CollisionGroup> collideWith)
		{
			if (Time.time >= lastTimeShot + currentWeaponData.CooldownTime)
			{
				lastTimeShot = Time.time;

				int bulletsToShoot = currentWeaponData.FireRate;

				if (currentWeaponData.ClipSize != WeaponData.InfiniteBullets)
				{
					bulletsToShoot = Mathf.Min (bulletsLeft, currentWeaponData.FireRate);
					bulletsLeft -= bulletsToShoot;
				}
				
				for (int i = 0; i < bulletsToShoot; i++)
				{
					float angle = transform.rotation.eulerAngles.z + Random.Range (-1f, 1f) * currentWeaponData.AngleRandomizer;
					eventDispatcher.Dispatch<SpawnBulletEvent> (new SpawnBulletEvent (transform.position, angle, collideWith, currentWeaponData.Damage));
				}

				if (dispatchStatusEvents)
				{
					eventDispatcher.Dispatch<AmmoUpdatedEvent> (new AmmoUpdatedEvent (bulletsLeft));
				}

				if (bulletsLeft <= 0 && currentWeaponData != defaultWeaponData)
				{
					SetWeaponData (defaultWeaponData);
				}
			}
		}

		public void SetWeaponData (WeaponData weaponData)
		{
			currentWeaponData = weaponData;

			bulletsLeft = weaponData.ClipSize;

			if (dispatchStatusEvents)
			{
				eventDispatcher.Dispatch<WeaponUpdatedEvent> (new WeaponUpdatedEvent (weaponData.Name));
				eventDispatcher.Dispatch<AmmoUpdatedEvent> (new AmmoUpdatedEvent (bulletsLeft));
			}
		}

		public bool IsDefaultWeapon ()
		{
			return currentWeaponData == defaultWeaponData;
		}

		public void Reset ()
		{
			SetWeaponData (defaultWeaponData);
		}
	}

}
using UnityEngine;
using Game.Model;
using Shared.DependencyInjection;
using Shared;

namespace Game.View.GameObjects.Ship
{
	[RequireComponent (typeof(Animator))]
	public class Teleporter: MonoBehaviour
	{
		private const string teleportingAnimatorParameterName = "teleporting";

		[Inject (Context.Game)]
		private GameModel gameModel;
	
		private Animator animator;

		private void Awake ()
		{
			animator = GetComponent<Animator> ();
			this.Inject ();
		}

		public void Teleport ()
		{
			Vector3 newPosition = Vector3.zero;

			newPosition.x = Random.Range (gameModel.CameraBounds.min.x, gameModel.CameraBounds.max.x);
			newPosition.y = Random.Range (gameModel.CameraBounds.min.y, gameModel.CameraBounds.max.y);

			transform.position = newPosition;

			animator.SetBool (teleportingAnimatorParameterName, true);
		}

		private void OnTeleportAnimationFinished ()
		{
			animator.SetBool (teleportingAnimatorParameterName, false);
		}
	}



}
using UnityEngine;
using System;
using Shared.Events;
using Game.Model;
using Game.Model.Scoring;
using Shared.DependencyInjection;
using Game.Event.UI;
using System.Collections.Generic;
using Shared;
using Game.Event;
using System.Collections;

namespace Game.View.GameObjects.Ship
{

	[RequireComponent (typeof(Rigidbody2D))]
	[RequireComponent (typeof(Weapon))]
	public class Spaceship : Spawnable, IDestructable
	{
		public Action<Spaceship> OnDied;

		public int Health { get; private set; }

		public int HealthRatio { get { return Health / startHealth; } }

		[Inject (Context.Game)]
		private EventDispatcher eventDispatcher;

		[Inject (Context.Game)]
		private GameModel gameModel;

		[Inject (Context.Game)]
		private ScoringConfiguration scoringConfiguration;

		[SerializeField]
		private float maxSpeed;

		[SerializeField]
		private CollisionGroup collisionGroup;

		[SerializeField]
		private Exhaust exhaust;

		[SerializeField]
		private DamageSmoke damageSmoke;

		[SerializeField]
		private int startHealth;

		[SerializeField]
		private SpriteRenderer spaceshipSprite;

		[SerializeField]
		private GameObject explosion;

		[SerializeField]
		[Range (0f, 100f)]
		private float damageForceMultiplier;

		[SerializeField]
		private CollisionGroup[] bulletsCollideWith;

		private float speed = 0f;

		private Rigidbody2D rigidBody;
		private Weapon weapon;

		private void Awake ()
		{
			rigidBody = GetComponent<Rigidbody2D> ();
			weapon = GetComponent<Weapon> ();

			this.Inject ();
			Health = startHealth;

			explosion.SetActive(false);
		}

		override public void Reset ()
		{
			Health = startHealth;
			speed = 0;
			exhaust.Speed = 0;
			damageSmoke.HealthRatio = 1f;
			weapon.Reset ();

			if (collisionGroup == CollisionGroup.Player)
			{
				eventDispatcher.Dispatch<PlayerHealthUpdatedEvent> (new PlayerHealthUpdatedEvent (Mathf.Clamp (Health, 0, startHealth), 0));
			}
		}

		public void Steer (float angle)
		{
			rigidBody.AddTorque (angle);
		}

		public void Throttle (float speed)
		{
			this.speed = Mathf.Clamp (speed, 0f, maxSpeed);
			exhaust.Speed = speed;
		}

		private void Update ()
		{
			rigidBody.AddForce (transform.rotation * Vector2.up * speed, ForceMode2D.Impulse);
		}

		public void Shoot ()
		{
			weapon.Shoot (new List<CollisionGroup> (bulletsCollideWith));
		}

		public void AddHealth (int healthBoost)
		{
			HandleHealthChange (healthBoost);
		}

		public bool IsUsingDefaultWeapon ()
		{
			return weapon.IsDefaultWeapon ();
		}

		private void HandleHealthChange (int healthChange)
		{
			int oldHealth = Health;

			Health += healthChange;
			Health = Mathf.Clamp (Health, 0, startHealth);

			if (collisionGroup == CollisionGroup.Player)
			{
				eventDispatcher.Dispatch<PlayerHealthUpdatedEvent> (new PlayerHealthUpdatedEvent (Health, Health - oldHealth));
			}

			damageSmoke.HealthRatio = (float)Health / (float)startHealth;

			if (Health <= 0)
			{
				if (OnDied != null)
				{
					OnDied (this);
				}
				if (collisionGroup == CollisionGroup.Enemy)
				{
					gameModel.AddScore (scoringConfiguration.GetScoreForDestroyedShip ());
				}
					
				gameModel.RemoveShip (this);

				StartCoroutine(Explode());
			}
		}

		private IEnumerator Explode ()
		{
			exhaust.Speed = 0;
			damageSmoke.HealthRatio = 1f;
			explosion.SetActive(true);
			yield return new WaitForSeconds(0.12f);
			spaceshipSprite.enabled = false;
			yield return new WaitForSeconds(1.2f);
			explosion.SetActive(false);
			spaceshipSprite.enabled = true;
			Pool.Return (gameObject);
		}

		private void OnCollisionEnter2D (Collision2D collision)
		{
			int damage = Mathf.RoundToInt (collision.relativeVelocity.magnitude * collision.rigidbody.mass * damageForceMultiplier);
			HandleHealthChange (-damage);
		}

		
		#region IDestructable implementation

		public CollisionGroup CollisionGroup
		{
			get
			{
				return collisionGroup;
			}
		}

		public void OnBulletHit (Vector3 bulletPosition, Vector2 hitDirection, int damage)
		{
			Health -= damage;
			HandleHealthChange (-damage);
		}

		#endregion
	}
}
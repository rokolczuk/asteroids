using UnityEngine;

namespace Game.View.GameObjects.Ship
{

	[RequireComponent (typeof(ParticleSystem))]
	public class DamageSmoke : MonoBehaviour
	{
		private ParticleSystem particles;

		[SerializeField]
		private float particleRateMultiplier;

		[SerializeField]
		private float startsizeMultiplier;

		public float HealthRatio
		{
			set
			{
				float damageRatio = 1f - Mathf.Clamp01 (value);

				var emissionModule = particles.emission;
				var emissionRate = emissionModule.rate;

				emissionRate.constant = startsizeMultiplier * particleRateMultiplier;
				emissionModule.rate = emissionRate;

				particles.startSize = damageRatio * startsizeMultiplier;

				if (damageRatio > 0f)
				{
					gameObject.SetActive (true);
				}
			}
		}

		private void Awake ()
		{
			particles = GetComponent<ParticleSystem> ();
			HealthRatio = 1f;
			gameObject.SetActive (false);
		}
	}
}
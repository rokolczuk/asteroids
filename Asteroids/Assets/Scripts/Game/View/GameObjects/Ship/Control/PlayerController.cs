using UnityEngine;
using Game.View.GameObjects.Ship;

namespace Game.View.GameObjects.Ship.Control
{
	[RequireComponent (typeof(Spaceship))]
	[RequireComponent (typeof(Teleporter))]
	public class PlayerController : MonoBehaviour
	{
		[SerializeField]
		private float rotationSpeedMultiplier;

		[SerializeField]
		private float accelerationMultiplier;

		private Spaceship ship;
		private Teleporter teleport;

		private bool firing = false;

		private void Awake ()
		{
			ship = GetComponent<Spaceship> ();
			ship.OnDied += OnShipDied;
			teleport = GetComponent<Teleporter> ();
		}

		private void Update ()
		{
			UpdateShipVelocity ();
			UpdateShipDirection ();
			UpdateShooting ();

			if (Input.GetButtonDown ("Fire2"))
			{
				teleport.Teleport ();
			}
		}

		private void UpdateShooting ()
		{
			if (Input.GetButtonDown ("Fire1"))
			{
				firing = true;
			}

			if (Input.GetButtonUp ("Fire1"))
			{
				firing = false;
			}

			if (firing)
			{
				ship.Shoot ();
			}
		}

		private void UpdateShipVelocity ()
		{
			float verticalInputAxis = Input.GetAxis ("Vertical");

			if (verticalInputAxis > 0f)
			{
				ship.Throttle (verticalInputAxis * accelerationMultiplier);
			}
		}

		private void UpdateShipDirection ()
		{
			float horizontalInputAxis = Input.GetAxis ("Horizontal");
			ship.Steer (horizontalInputAxis * rotationSpeedMultiplier);
		}

		private void OnShipDied (Spaceship obj)
		{
			firing = false;
		}
	}

}
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Game.Model;
using Shared.DependencyInjection;
using Game.Model.Powerups;
using Shared;

namespace Game.View.GameObjects.Ship.Control
{
﻿/***
 *  Note from author: I didn't have time to clean this up. If I did though I'd break down each strategy to it's own class. 
 **/


	public enum EnemyStrategy
	{
		AttackingPlayer,
		CollectingPowerups,
		Idle
	}

	[RequireComponent (typeof(Spaceship))]
	public class EnemyController : MonoBehaviour
	{
		[Inject (Context.Game)]
		private GameModel gameModel;
	
		[SerializeField]
		private float rotationSpeedMultiplier;

		[SerializeField]
		private float accelerationMultiplier;

		[SerializeField]
		[Range (0f, 15f)]
		private float shootAngle;

		[SerializeField]
		[Range (0f, 15f)]
		private float throttleAngle;

		[SerializeField]
		[Range (0.1f, 3f)]
		private float updateAvoidVectorInterval;

		[SerializeField]
		[Range (0.1f, 3f)]
		private float updateStrategyInterval;

		[SerializeField]
		[Range (0.1f, 5f)]
		private float minAvoidDistance;

		[SerializeField]
		[Range (0.1f, 20f)]
		private float avoidVectorMultiplier;

		[SerializeField]
		[Range (0.1f, 20f)]
		private float maxPlayerTargetFollowVectorMagnitude;

		[SerializeField]
		[Range (0.1f, 20f)]
		private float maxCollectibleFollowVectorMagnitude;

		[SerializeField]
		[Range (0.0f, 1f)]
		private float minHealthRadioToFollowHealthPacks;

		[SerializeField]
		[Range (0.0f, 1f)]
		private float minSpeedWhenCollectingItems;

		[SerializeField]
		private EnemyStrategy strategy = EnemyStrategy.Idle;

		private float timeAvoidVectorUpdated;
		private float timeStrategyUpdated;

		private Spaceship ship;
		private Transform targetTransform;

		private Vector3 playerTargetPosition;
		private Vector3 avoidObstaclesPosition = Vector3.zero;
		private Vector3 targetPosition;

		private float angleToPositionTarget;
		private float angleToAttackTarget;

		private int numObstaclesInDangerZone;

		private Collectible pursuedCollectible;

		private void Awake ()
		{
			this.Inject ();
			ship = GetComponent<Spaceship> ();
			ship.OnDied += OnDied;
			timeStrategyUpdated = timeAvoidVectorUpdated = 0f;
		}

		private void Update ()
		{
			TryUpdateStrategy ();
			AvoidObstacles ();
			FollowTarget ();
			UpdateTargetPosition ();
			UpdateShipDirection ();
			UpdateShipVelocity ();
			ShootIfSeePlayer ();
		}

		private void TryUpdateStrategy ()
		{
			if (Time.time >= timeStrategyUpdated + updateStrategyInterval)
			{
				UpdateStrategy ();
			}
		}

		private void UpdateStrategy ()
		{
			if (strategy != EnemyStrategy.CollectingPowerups)
			{
				strategy = EnemyStrategy.Idle;

				ReadOnlyCollection<Collectible> collectibles = gameModel.Collectibles;

				if (collectibles.Count > 0 && ship.IsUsingDefaultWeapon ())
				{
					for (int i = 0; i < collectibles.Count; i++)
					{
						Collectible collectible = collectibles [i];

						if (!collectible.IsBeingCollected && IsWorthCollecting (collectible))
						{
							collectible.IsBeingCollected = true;
							collectible.OnCollected += OnCollectibleCollected;
							targetTransform = collectible.transform;
							strategy = EnemyStrategy.CollectingPowerups;
							pursuedCollectible = collectible;
							break;
						}
					}
				}

				if (strategy != EnemyStrategy.CollectingPowerups && gameModel.PlayerShip != null)
				{
					strategy = EnemyStrategy.AttackingPlayer;
					targetTransform = gameModel.PlayerShip.transform;
				}
			}

			timeStrategyUpdated = Time.time;
		}

		private bool IsWorthCollecting (Collectible collectible)
		{
			return collectible.PowerupType == PowerupType.Weapon && ship.IsUsingDefaultWeapon () ||
			collectible.PowerupType == PowerupType.HealthPack && ship.HealthRatio <= minHealthRadioToFollowHealthPacks;
		}

		private void OnDied (Spaceship ship)
		{
			if (pursuedCollectible != null)
			{
				pursuedCollectible.OnCollected -= OnCollectibleCollected;
				pursuedCollectible = null;
			}

			strategy = EnemyStrategy.Idle;
		}

		private void OnCollectibleCollected (Collectible collectible)
		{
			collectible.OnCollected -= OnCollectibleCollected;
			collectible.IsBeingCollected = false;
			collectible = null;
			strategy = EnemyStrategy.Idle;
			UpdateStrategy ();
		}

		private void AvoidObstacles ()
		{
			if (Time.time >= timeAvoidVectorUpdated + updateAvoidVectorInterval)
			{
				timeAvoidVectorUpdated = Time.time;
				avoidObstaclesPosition = Vector3.zero;

				float closestObstacleDistance = 1000f;
				numObstaclesInDangerZone = 0;

				ReadOnlyCollection<Spawnable> obstacles = gameModel.Obstacles;

				for (int i = 0; i < obstacles.Count; i++)
				{
					float distanceToObstacle = Vector3.Distance (transform.position, obstacles [i].transform.position);

					if (obstacles [i] != ship && distanceToObstacle < minAvoidDistance)
					{
						numObstaclesInDangerZone++;
						closestObstacleDistance = Mathf.Min (distanceToObstacle, closestObstacleDistance);
						avoidObstaclesPosition += (transform.position - obstacles [i].transform.position).normalized;
						
					}
				}

				avoidObstaclesPosition /= numObstaclesInDangerZone;

				avoidObstaclesPosition *= (minAvoidDistance - closestObstacleDistance) * avoidVectorMultiplier;
				avoidObstaclesPosition += transform.position;
			}

			if (numObstaclesInDangerZone > 0)
			{
				Debug.DrawLine (transform.position, avoidObstaclesPosition, Color.yellow);
			}
		}

		private void FollowTarget ()
		{
			if (targetTransform != null)
			{
				float distanceToPlayer = Vector3.Distance (targetTransform.position, transform.position);
				float maxVectorMagnitude = strategy == EnemyStrategy.CollectingPowerups ? maxCollectibleFollowVectorMagnitude : maxPlayerTargetFollowVectorMagnitude;
				float followVectorMagintude = Mathf.Min (distanceToPlayer, maxVectorMagnitude);

				playerTargetPosition = Vector3.Lerp (transform.position, targetTransform.position, followVectorMagintude / distanceToPlayer);
			} else
			{
				playerTargetPosition = transform.position;
			}

			Debug.DrawLine (transform.position, playerTargetPosition, Color.blue);
		}

		private void ShootIfSeePlayer ()
		{
			if (strategy == EnemyStrategy.AttackingPlayer && Mathf.Abs (angleToAttackTarget) < shootAngle)
			{
				ship.Shoot ();
			}
		}

		private void UpdateTargetPosition ()
		{
			if (numObstaclesInDangerZone > 0)
			{
				targetPosition = playerTargetPosition + avoidObstaclesPosition - transform.position;
			} else
			{
				targetPosition = playerTargetPosition;
			}

			Debug.DrawLine (transform.position, targetPosition);
		}

		private void UpdateShipVelocity ()
		{
			if (Mathf.Abs (angleToPositionTarget) < throttleAngle)
			{
				float distanceToTarget = Vector3.Distance (transform.position, targetPosition);
				float speed = distanceToTarget * accelerationMultiplier;

				if (strategy == EnemyStrategy.CollectingPowerups)
				{
					speed = Mathf.Max (minSpeedWhenCollectingItems, speed);
				}
				ship.Throttle (speed);
			}
		}

		private void UpdateShipDirection ()
		{
			angleToPositionTarget = GetRelativeAngleToPosition (targetPosition);
			angleToAttackTarget = GetRelativeAngleToPosition (playerTargetPosition);

			ship.Steer (angleToPositionTarget * rotationSpeedMultiplier);
		}

		private float GetRelativeAngleToPosition (Vector3 target)
		{
			Vector3 relativePositionToTarget = targetPosition - transform.position;
			Vector3 rotation = transform.rotation * Vector3.up;

			float angle = Vector3.Angle (relativePositionToTarget, rotation);
			Vector3 cross = Vector3.Cross (relativePositionToTarget, rotation);

			if (cross.z < 0)
			{
				angle = -angle;
			}

			return angle;
		}

		private void OnDestroy ()
		{
			ship.OnDied -= OnDied;
		}
	}
}
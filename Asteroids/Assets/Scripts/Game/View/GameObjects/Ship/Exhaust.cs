using UnityEngine;

namespace Game.View.GameObjects.Ship
{

	[RequireComponent (typeof(ParticleSystem))]
	public class Exhaust : MonoBehaviour
	{

		private ParticleSystem particles;

		[SerializeField]
		private float particleRateMultiplier;

		[SerializeField]
		private float minExhaustActivateSpeed;

		public float Speed
		{
			set
			{
				if (value < minExhaustActivateSpeed)
				{
					value = 0;
				}

				var emissionModule = particles.emission;
				var emissionRate = emissionModule.rate;

				emissionRate.constant = value * particleRateMultiplier;
				emissionModule.rate = emissionRate;
			}
		}

		private void Awake ()
		{
			particles = GetComponent<ParticleSystem> ();
		}
	}
}
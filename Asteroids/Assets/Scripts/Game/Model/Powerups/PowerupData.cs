using System;
using UnityEngine;
using Game.Model.Weapons;
using Game.View.GameObjects.Ship;

namespace Game.Model.Powerups
{
	public enum PowerupType
	{
		Weapon,
		HealthPack
	}

	[Serializable]
	public class PowerupData
	{
		public PowerupType Type;
		public Sprite Sprite;

		[SerializeField]
		private int healthBoost;

		[SerializeField]
		private WeaponData weaponData;

		public virtual void Apply (Spaceship ship)
		{
			if (Type == PowerupType.HealthPack)
			{
				ship.AddHealth (healthBoost);
			} else if (Type == PowerupType.Weapon)
			{
				Weapon weapon = ship.GetComponent<Weapon> ();
				weapon.SetWeaponData (weaponData);
			} else
			{
				Debug.LogError ("Unrecognized powerup type");
			}
		}
	}
}
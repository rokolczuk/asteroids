namespace Game.Model.Powerups { 
﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;

[CreateAssetMenu(menuName="Asteroids/Powerups", fileName="Powerups.asset")]
public class Powerups: ScriptableObject
{
	[SerializeField]
	private List<PowerupData> powerups;

	public PowerupData GetRandomPowerup()
	{
		Assert.IsTrue(powerups.Count > 0);
		return powerups[Random.Range(0, powerups.Count)];
	}
}
}
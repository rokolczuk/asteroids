using System;
using System.Collections.Generic;

namespace Game.Model.Levels
{

	[Serializable]
	public class LevelData
	{
		public List<int> asteroidSizes;

		public bool HasPowerups;
		public float PowerupSpawnIntervalTime;

		public EnemyConfiguration EnemyConfiguration;
		public string Tutorial;
	}
}
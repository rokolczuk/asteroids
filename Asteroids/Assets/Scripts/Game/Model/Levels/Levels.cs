using UnityEngine;
using System.Collections.Generic;

namespace Game.Model.Levels
{

	[CreateAssetMenu (menuName = "Asteroids/Levels List", fileName = "Levels.asset")]
	public class Levels: ScriptableObject
	{
		[SerializeField]
		private List<LevelData> levels;

		public int NumLevels
		{
			get
			{
				return levels.Count;			
			}
		}

		public LevelData GetLevel (int levelIndex)
		{
			return levels [levelIndex];
		}
	}
}
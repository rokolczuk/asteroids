using UnityEngine;

namespace Game.Model.Weapons
{
	[CreateAssetMenu (menuName = "Asteroids/New Weapon", fileName = "Weapon.asset")]
	public class WeaponData: ScriptableObject
	{
		public const int InfiniteBullets = -1;

		public string Name;
		public int FireRate;
		public float AngleRandomizer;
		public float CooldownTime;
		public int Damage;
		public int ClipSize;

	}
}
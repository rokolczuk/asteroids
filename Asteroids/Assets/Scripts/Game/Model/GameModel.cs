using Game.View.GameObjects.Ship;
using Game.View.GameObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Shared.Events;
using Game.Event.UI;
using Shared.DependencyInjection;
using Shared;

namespace Game.Model
{

	public class GameModel
	{
		public int Score { get; private set; }

		public int CurrentLevel { get; private set; }

		public int Health { get; private set; }

		public bool GameOver { get; private set; }

		public Spaceship PlayerShip { get; private set; }

		public ReadOnlyCollection<Asteroid> Asteroids { get { return asteroids.AsReadOnly (); } }

		public ReadOnlyCollection<Spaceship> Ships { get { return ships.AsReadOnly (); } }

		public ReadOnlyCollection<Spawnable> Obstacles { get { return obstacles.AsReadOnly (); } }

		public ReadOnlyCollection<Collectible> Collectibles { get { return collectibles.AsReadOnly (); } }

		public Bounds CameraBounds;

		[Inject (Context.Game)]
		private EventDispatcher EventDispatcher;

		private List<Asteroid> asteroids = new List<Asteroid> ();
		private List<Spaceship> ships = new List<Spaceship> ();
		private List<Collectible> collectibles = new List<Collectible> ();
		private List<Spawnable> spawnables = new List<Spawnable> ();
		private List<Spawnable> obstacles = new List<Spawnable> ();


		private bool gameOver;

		public GameModel ()
		{
			this.Inject ();
		}

		public void AddScore (int score)
		{
			Score += score;
			EventDispatcher.Dispatch<ScoreUpdatedEvent> (new ScoreUpdatedEvent (Score));
		}

		public void Reset ()
		{
			Score = 0;
			CurrentLevel = 0;
			GameOver = false;

			EventDispatcher.Dispatch<ScoreUpdatedEvent> (new ScoreUpdatedEvent (Score));
		}

		public void CompleteLevel ()
		{
			CurrentLevel++;
		}

		public void AddAsteroid (Asteroid asteroid)
		{
			asteroids.Add (asteroid);
			spawnables.Add (asteroid);
			obstacles.Add (asteroid);
		}

		public void RemoveAsteroid (Asteroid asteroid)
		{
			asteroids.Remove (asteroid);
			spawnables.Remove (asteroid);
			obstacles.Remove (asteroid);
		}

		public void AddShip (Spaceship ship, bool isPlayer)
		{
			ships.Add (ship);
			spawnables.Add (ship);
			obstacles.Add (ship);

			if (isPlayer)
			{
				PlayerShip = ship;
			}
		}

		public void RemoveShip (Spaceship ship)
		{
			ships.Remove (ship);
			spawnables.Remove (ship);
			obstacles.Remove (ship);

			if (ship == PlayerShip)
			{
				PlayerShip = null;
				GameOver = true;
			}
		}

		public void AddCollectible (Collectible collectible)
		{
			collectibles.Add (collectible);
			spawnables.Add (collectible);
		}

		public void RemoveCollectible (Collectible collectible)
		{
			collectibles.Remove (collectible);
			spawnables.Remove (collectible);
		}

		public Vector3 GetRandomAvailablePosition (float margin = 0)
		{
			bool lookingForPosition = true;
			Vector3 position = Vector3.zero;
			Bounds cameraBounds = CameraBounds;

			int numAttemps = 0;
			const int maxNumAttepts = 30;

			while (lookingForPosition)
			{
				lookingForPosition = false;

				position.x = margin + Random.Range (cameraBounds.min.x + margin, cameraBounds.max.x - margin);
				position.y = margin + Random.Range (cameraBounds.min.y + margin, cameraBounds.max.y - margin);

				for (int i = 0; i < spawnables.Count; i++)
				{
					if (Vector3.Distance (position, spawnables [i].transform.position) < spawnables [i].BlockSpawningInRadius)
					{
						lookingForPosition = true;
						break;
					}
				}

				if (++numAttemps >= maxNumAttepts)
				{
					Debug.Log ("Couldn't find an available location in a reasonable time");
					//this could be improved by using some more sophisticated data structure like quadtree
				}
			}

			return position;

		}
	}
}
﻿using System;

namespace Game.Model.Levels
{
	[Serializable]
	public class EnemyConfiguration
	{
		public bool SpawnEnemiesAtStart;
		public bool SpawnEnemiesInInterval;
		public int NumEnemiesAtStart;
		public float FirstEnemyWaveDelay;
		public float EnemySpawnInterval;
	}
}


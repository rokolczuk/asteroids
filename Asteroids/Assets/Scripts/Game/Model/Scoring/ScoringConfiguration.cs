using UnityEngine;

namespace Game.Model.Scoring
{
	[CreateAssetMenu (menuName = "Asteroids/Scoring Configuration", fileName = "Scoring.asset")]
	public class ScoringConfiguration: ScriptableObject
	{
		[SerializeField]
		private int mediumAsteroidSize = 4;

		[SerializeField]
		private int largeAsteroidSize = 6;

		[SerializeField]
		private int smallAsteroidScore = 20;

		[SerializeField]
		private int mediumAsteroidScore = 50;

		[SerializeField]
		private int largeAsteroidScore = 100;

		[SerializeField]
		private int enemyShipDestroyedScore = 200;

		public int GetScoreForAsteroidSize (int size)
		{
			int score = smallAsteroidScore;

			if (size >= largeAsteroidSize)
			{
				score = largeAsteroidScore;
			} else if (size >= mediumAsteroidSize)
			{
				score = mediumAsteroidScore;
			}

			return score;
		}

		public int GetScoreForDestroyedShip ()
		{
			return enemyShipDestroyedScore;
		}
	}
}
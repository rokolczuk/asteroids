namespace Game.Model
{
	public enum CollisionGroup
	{
		Player,
		Enemy,
		Asteroids
	}
}
using UnityEngine;
using Game.Model.Powerups;

namespace Game.Event
{
	public class SpawnPowerupEvent
	{
		public readonly Vector3 Position;
		public readonly PowerupData PowerupData;

		public SpawnPowerupEvent (PowerupData powerupData, Vector3 position)
		{
			this.PowerupData = powerupData;
			this.Position = position;
		}
	}
}
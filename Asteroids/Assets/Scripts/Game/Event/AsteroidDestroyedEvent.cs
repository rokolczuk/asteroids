using Game.View.GameObjects;

namespace Game.Event
{

	public class AsteroidDestroyedEvent
	{
		public readonly Asteroid Asteroid;

		public AsteroidDestroyedEvent (Asteroid asteroid)
		{
			this.Asteroid = asteroid;
		}
	}



}
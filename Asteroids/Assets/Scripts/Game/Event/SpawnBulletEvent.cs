﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Game.Model;


namespace Game.Event
{
	public class SpawnBulletEvent
	{
		public readonly Vector3 Position;
		public readonly float Angle;
		public ReadOnlyCollection<CollisionGroup> CollideWith;
		public readonly int Damage;

		public SpawnBulletEvent (Vector3 position, float angle, List<CollisionGroup> collideWith, int damage)
		{
			this.Position = position;
			this.Angle = angle;
			this.CollideWith = collideWith.AsReadOnly ();
			this.Damage = damage;
		}
	}

}
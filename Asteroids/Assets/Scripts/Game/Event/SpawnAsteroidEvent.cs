using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Game.Event
{
	public class SpawnAsteroidEvent
	{
		public readonly Vector3 Position;
		public readonly float Angle;
		public readonly int Size;

		public SpawnAsteroidEvent (Vector3 position, float angle, int size)
		{
			this.Position = position;
			this.Angle = angle;
			this.Size = size;
		}
	}
}
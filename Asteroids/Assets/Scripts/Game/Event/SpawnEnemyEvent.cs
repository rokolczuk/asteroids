using UnityEngine;

namespace Game.Event
{
	public class SpawnEnemyEvent
	{
		public readonly Vector3 Position;

		public SpawnEnemyEvent (Vector3 position)
		{
			this.Position = position;
		}
	}
}
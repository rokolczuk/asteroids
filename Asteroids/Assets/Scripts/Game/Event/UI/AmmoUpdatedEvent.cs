namespace Game.Event.UI 
{
	﻿public class AmmoUpdatedEvent
	{
		public readonly int Ammo;

		public AmmoUpdatedEvent (int ammo)
		{
			this.Ammo = ammo;
		}
	}
}
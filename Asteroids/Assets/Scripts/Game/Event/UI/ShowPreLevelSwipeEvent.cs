namespace Game.Event.UI
{
	public class ShowPreLevelSwipeEvent
	{
		public readonly int Level;
		public readonly string Tutorial;

		public ShowPreLevelSwipeEvent (int level, string tutorial)
		{
			this.Level = level;
			this.Tutorial = tutorial;
		}
	}
}
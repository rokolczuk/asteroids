using System;

namespace Game.Event.UI
{

	public class PlayerHealthUpdatedEvent
	{
		public readonly int Health;
		public readonly int HealthChange;

		public PlayerHealthUpdatedEvent (int health, int healthChange)
		{
			this.Health = health;
			this.HealthChange = healthChange;
		}
	}
}
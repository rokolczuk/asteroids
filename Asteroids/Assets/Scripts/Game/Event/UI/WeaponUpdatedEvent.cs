namespace Game.Event.UI
{
﻿public class WeaponUpdatedEvent
	{
		public readonly string WeaponName;

		public WeaponUpdatedEvent (string weaponName)
		{
			this.WeaponName = weaponName;
		}
	}
}
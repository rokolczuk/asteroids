﻿using System;

namespace Game.Event.UI
{
	public class ScoreUpdatedEvent
	{
		public readonly int Score;

		public ScoreUpdatedEvent (int score)
		{
			this.Score = score;
		}
	}
}
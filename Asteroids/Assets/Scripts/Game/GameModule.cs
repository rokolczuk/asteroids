﻿using System;
using UnityEngine;
using Shared;
using Shared.DependencyInjection;
using Shared.Events;
using Game.Model;
using Game.Model.Levels;
using Game.Model.Scoring;
using Game.Model.Powerups;

namespace Game
{
	public class GameModule : IModule
	{
		private InjectProvider injectProvider;

		#region IModule implementation

		public void Initialise ()
		{
			InjectProvider.CreateProvider (Context.Game);
			injectProvider = InjectProvider.GetProvider (Context.Game);

			injectProvider.Map<EventDispatcher> ();
			injectProvider.Map<GameModel> ();
			injectProvider.MapInstance<Levels> (Resources.Load<Levels> ("Levels"));
			injectProvider.MapInstance<ScoringConfiguration> (Resources.Load<ScoringConfiguration> ("Scoring"));
			injectProvider.MapInstance<Powerups> (Resources.Load<Powerups> ("Powerups"));
		}

		public void Dispose ()
		{
			injectProvider.Destroy ();
		}

		public string GetSceneName ()
		{
			return "game";
		}

		#endregion
	}


}
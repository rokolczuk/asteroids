using Shared;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Shell
{
	public class ModuleManager
	{
		private IModule currentModule;

		public ModuleManager ()
		{
		}

		public void LoadModule (IModule module)
		{
			if (currentModule != module)
			{
				if (currentModule != null)
				{
					currentModule.Dispose ();
				}

				currentModule = module;
				currentModule.Initialise ();

				SceneManager.LoadScene (currentModule.GetSceneName ());
			} 
			else
			{
				Debug.LogWarning ("Module already loaded");
			}
		}
	}
}
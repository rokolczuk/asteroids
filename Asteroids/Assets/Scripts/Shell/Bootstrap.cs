using UnityEngine;
using Shared.DependencyInjection;
using Game;
using Menu;
using Shared.Events;
using Shared;
using Menu.Event;
using Game.Event;

namespace Shell
{
	public class Bootstrap: MonoBehaviour
	{
		private InjectProvider injectProvider;

		private GameModule gameModule = new GameModule ();
		private MenuModule menuModule = new MenuModule ();

		private ModuleManager moduleManager = new ModuleManager ();

		private EventDispatcher eventDispatcher;

		private void Awake ()
		{
			CreateMappings ();
			InitialiseEvents ();
			moduleManager.LoadModule (menuModule);
		}

		private void CreateMappings ()
		{
			InjectProvider.CreateProvider (Context.Shell);
			injectProvider = InjectProvider.GetProvider (Context.Shell);
			injectProvider.Map<EventDispatcher> ();

			eventDispatcher = injectProvider.GetInstance<EventDispatcher> ();
		}

		private void InitialiseEvents ()
		{
			eventDispatcher.AddEventListener<LaunchGameEvent> (OnLaunchGame);
			eventDispatcher.AddEventListener<LaunchMenuEvent> (OnLaunchMenu);
		}

		private void OnLaunchGame (LaunchGameEvent e)
		{
			moduleManager.LoadModule (gameModule);
		}

		private void OnLaunchMenu (LaunchMenuEvent e)
		{
			moduleManager.LoadModule (menuModule);
		}

		private void OnDestroy ()
		{
			eventDispatcher.RemoveEventListener<LaunchGameEvent> (OnLaunchGame);
			eventDispatcher.RemoveEventListener<LaunchMenuEvent> (OnLaunchMenu);
		}
	}


}
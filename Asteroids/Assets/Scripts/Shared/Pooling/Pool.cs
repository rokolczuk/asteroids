using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;

namespace Shared.Pooling
{
	public class Pool : MonoBehaviour
	{
		[SerializeField]
		private GameObject prefab;

		[SerializeField]
		private int maxInstances;

		[SerializeField]
		private bool fillPoolOnStartup;

		[SerializeField]
		private bool autoGrow;

		private List<GameObject> availableInstances = new List<GameObject> ();
		private List<GameObject> instances = new List<GameObject> ();

		public int MaxInstances
		{
			get
			{
				return maxInstances;
			}
		}

		public int NumAvailableInstances
		{
			get
			{
				return availableInstances.Count;
			}
		}

		private void Awake ()
		{
			if (fillPoolOnStartup)
			{
				while (instances.Count < maxInstances)
				{
					CreateInstance ();
				}
			}
		}

		private void CreateInstance ()
		{
			GameObject gameObject = GameObject.Instantiate (prefab) as GameObject;
			instances.Add (gameObject);
			AddToPool (gameObject);
		}

		public T Get<T> ()
		{
			if (availableInstances.Count == 0 && !autoGrow)
			{
				throw new UnityException ("Cannot get an instance");
			} else if (availableInstances.Count == 0 && autoGrow)
			{
				maxInstances++;
				CreateInstance ();
			}

			GameObject instance = availableInstances [0];
			availableInstances.RemoveAt (0);
			instance.SetActive (true);

			ISpawnedByPool spawnedByPool = instance.GetComponent<ISpawnedByPool> ();
			spawnedByPool.Pool = this;
			spawnedByPool.Reset ();

			return instance.GetComponent<T> ();
		}

		public void Return (GameObject instance)
		{
			Assert.IsTrue (instances.Contains (instance), "Attempted to return an object that wasn't created by pool");

			AddToPool (instance);

		}

		private void AddToPool (GameObject instance)
		{
			instance.SetActive (false);
			availableInstances.Add (instance);
			instance.transform.SetParent (transform);
		}
	}

}
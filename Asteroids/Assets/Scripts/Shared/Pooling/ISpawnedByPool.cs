namespace Shared.Pooling
{
	public interface ISpawnedByPool
	{
		Pool Pool { get; set; }

		void Reset ();
	}
}
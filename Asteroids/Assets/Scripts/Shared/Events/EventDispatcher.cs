using System.Collections.Generic;
using System;
using Shared.DependencyInjection;

namespace Shared.Events
{
	public class EventDispatcher
	{

		private Dictionary<Type, List<Delegate>> dictionary = new Dictionary<Type, List<Delegate>> ();

		public delegate void EventHandler<TEvent> (TEvent e);

		public void AddEventListener<TEvent> (EventHandler<TEvent> eventHandler)
		{
			Type type = typeof(TEvent);

			if (!dictionary.ContainsKey (type))
			{
				dictionary.Add (type, new List<Delegate> ());
			}
	
			dictionary [type].Add (eventHandler);
		}

		public void RemoveEventListener<TEvent> (EventHandler<TEvent> eventHandler)
		{
			Type type = typeof(TEvent);

			if (dictionary.ContainsKey (type))
			{
				dictionary [type].Remove (eventHandler);
			}
		}

		public void Dispatch<T> (T eventInstance)
		{
			InternalDispatch<T> (eventInstance, true);
		}

		private void InternalDispatch<T> (T eventInstance, bool forward)
		{
			Type type = typeof(T);

			if (dictionary.ContainsKey (type))
			{
				List<Delegate> listeners = dictionary [type];

				for (int i = 0; i < listeners.Count; i++)
				{
					listeners [i].DynamicInvoke (eventInstance);
				}
			}

			if (forward)
			{
				ForwardEvent (eventInstance, type);
			}
		}

		private void ForwardEvent<T> (T eventInstance, Type type)
		{
			object[] attributes = type.GetCustomAttributes (typeof(SharedEventAttribute), true);

			for (int i = 0; i < attributes.Length; i++)
			{
				if (attributes [i] is SharedEventAttribute)
				{
					SharedEventAttribute sharedEventAttribute = attributes [i] as SharedEventAttribute;
					InjectProvider injectProvider = InjectProvider.GetProvider (sharedEventAttribute.ShareToContext);
					EventDispatcher eventDispatcher = injectProvider.GetInstance<EventDispatcher> ();
					eventDispatcher.InternalDispatch<T> (eventInstance, false);
				}
			}
		}
	}
}
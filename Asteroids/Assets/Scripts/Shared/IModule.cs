namespace Shared
{
	public interface IModule
	{
		void Initialise ();

		void Dispose ();

		string GetSceneName ();
	}
}
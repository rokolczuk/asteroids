using System;

namespace Shared.DependencyInjection
{
	[AttributeUsage (AttributeTargets.Field)]
	public class InjectAttribute: Attribute
	{
		public readonly Context Context;

		public InjectAttribute (Context context)
		{
			this.Context = context;
		}
	}
}
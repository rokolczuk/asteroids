using UnityEngine;
using System;
using System.Reflection;

namespace Shared.DependencyInjection
{
	public static class Injection
	{
		//used by unit tests
		private static bool testMode = false;

		public static bool TestMode
		{
		#if UNITY_EDITOR
			set
			{
				testMode = value;
			}
		#endif 
			get
			{
				return testMode;
			}
		}

		public static void Inject (this object requestingObject)
		{
			if (!Application.isPlaying && !testMode)
			{
				return;
			}

			Type type = requestingObject.GetType ();

			FieldInfo[] fields = type.GetFields (BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);

			for (int i = 0; i < fields.Length; i++)
			{
				FieldInfo field = fields [i];

				if (Attribute.IsDefined (field, typeof(InjectAttribute)))
				{
					InjectAttribute injectAttribute = (InjectAttribute)Attribute.GetCustomAttribute (field, typeof(InjectAttribute));
					InjectProvider provider = InjectProvider.GetProvider (injectAttribute.Context);

					object value = provider.GetInstance (field.FieldType);

					field.SetValue (requestingObject, value);
				}
			}
		}
	}
}
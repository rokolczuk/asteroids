using UnityEngine;

namespace Shared
{
	public class PersistantComponent : MonoBehaviour
	{
		private void Awake ()
		{
			DontDestroyOnLoad (gameObject);
		}
	}
}
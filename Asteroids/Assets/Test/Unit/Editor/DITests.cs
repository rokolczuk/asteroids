﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using Shared.DependencyInjection;
using Shared;

public interface ITestInterface
{
}

public class Foo: ITestInterface
{
}

public class Bar: ITestInterface
{
	
}

public class Injectable
{
	[Inject(Context.Shell)]
	public ITestInterface testField;

	public Injectable()
	{
		this.Inject();
	}
}

public class DITests 
{
	private void Prepare ()
	{
		Injection.TestMode = true;

		if(InjectProvider.HasProvider(Context.Shell))
		{
			InjectProvider.DestroyProvider(Context.Shell);
		}

		InjectProvider.CreateProvider(Context.Shell);
	}

	private void Dispose ()
	{
		Injection.TestMode = false;
		InjectProvider.DestroyProvider(Context.Shell);
	}

	[Test]
	public void TestSimpleTypeMapping()
	{
		Prepare();
		InjectProvider testInjectProvider = InjectProvider.GetProvider(Context.Shell);

		Assert.IsFalse(testInjectProvider.HasMapping<Foo>());
		testInjectProvider.Map<Foo>();
		Assert.IsTrue(testInjectProvider.HasMapping<Foo>());
		Assert.IsNotNull(testInjectProvider.GetInstance<Foo>());

		Dispose ();
	}

	[Test]
	public void TestTypeToTypeMapping()
	{
		Prepare();
		InjectProvider testInjectProvider = InjectProvider.GetProvider(Context.Shell);

		Assert.IsFalse(testInjectProvider.HasMapping<ITestInterface>());
		Assert.IsFalse(testInjectProvider.HasMapping<Foo>());
		testInjectProvider.Map<ITestInterface, Foo>();
		Assert.IsTrue(testInjectProvider.HasMapping<ITestInterface>());
		Assert.IsFalse(testInjectProvider.HasMapping<Foo>());
	
		Dispose ();
	}
		
	[Test]
	public void TestInstanceMapping()
	{
		Prepare();
		InjectProvider testInjectProvider = InjectProvider.GetProvider(Context.Shell);

		Bar bar = new Bar();

		Assert.IsFalse(testInjectProvider.HasMapping<Bar>());
		testInjectProvider.MapInstance<Bar>(bar);
		Assert.IsTrue(testInjectProvider.HasMapping<Bar>());
		Assert.IsFalse(testInjectProvider.HasMapping<Foo>());

		Dispose ();
	}

	[Test]
	public void TestInjectedMember()
	{
		Prepare();
		InjectProvider testInjectProvider = InjectProvider.GetProvider(Context.Shell);

		testInjectProvider.Map<ITestInterface, Bar>();
		testInjectProvider.Map<Injectable>();

		Injectable instance = testInjectProvider.GetInstance<Injectable>() as Injectable;

		Assert.IsNotNull(instance);
		Assert.IsNotNull(instance.testField);
		Assert.IsTrue(instance.testField is Bar);

		Dispose ();
	}

	[Test]
	public void TestIfSameInstanceIsReturned()
	{
		Prepare();
		InjectProvider testInjectProvider = InjectProvider.GetProvider(Context.Shell);


		testInjectProvider.Map<Foo>();

		Foo instance = testInjectProvider.GetInstance<Foo>() as Foo;
		Foo instance2 = testInjectProvider.GetInstance<Foo>() as Foo;

		Assert.AreEqual(instance, instance2);

		Bar bar = new Bar();
		testInjectProvider.MapInstance<Bar>(bar);

		Bar instance3 = testInjectProvider.GetInstance<Bar>() as Bar;
		Bar instance4 = testInjectProvider.GetInstance<Bar>() as Bar;

		Assert.AreEqual(instance3, instance4);

		Dispose ();
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;
using System.Collections.Generic;
using Shared.Pooling;

[RequireComponent(typeof(Pool))]
public class PoolingTest : MonoBehaviour 
{
	private Pool pool;

	private void Start()
	{
		pool = GetComponent<Pool>();

		TestOneInstance();
		TestGrow();

		IntegrationTest.Pass();
	}

	private void TestOneInstance ()
	{
		Assert.IsTrue(Component.FindObjectsOfType<TestablePooledObject>().Length == 0);

		int poolCapacity = pool.MaxInstances;
		int numAvailableInstances = pool.NumAvailableInstances;

		Assert.IsTrue(poolCapacity > 0);
		Assert.IsTrue(numAvailableInstances > 0);

		TestablePooledObject createdObject = pool.Get<TestablePooledObject>();

		Assert.IsTrue(numAvailableInstances - 1 == pool.NumAvailableInstances);

		pool.Return(createdObject.gameObject);

		Assert.IsTrue(numAvailableInstances == pool.NumAvailableInstances);

		Assert.IsTrue(Component.FindObjectsOfType<TestablePooledObject>().Length == 0);
	}

	private void TestGrow ()
	{
		Assert.IsTrue(Component.FindObjectsOfType<TestablePooledObject>().Length == 0);

		int poolCapacity = pool.MaxInstances;
		int numAvailableInstances = pool.NumAvailableInstances;

		Assert.IsTrue(poolCapacity > 0);
		Assert.IsTrue(numAvailableInstances > 0);

		List<TestablePooledObject> createdObjects = new List<TestablePooledObject>();

		for(int i = 0; i < poolCapacity * 2; i++)
		{
			createdObjects.Add(pool.Get<TestablePooledObject>());
		}

		Assert.IsTrue(pool.NumAvailableInstances == 0);
		Assert.IsTrue(Component.FindObjectsOfType<TestablePooledObject>().Length == poolCapacity * 2);

		for(int i = 0; i < createdObjects.Count; i++)
		{
			pool.Return(createdObjects[i].gameObject);
		}

		createdObjects.Clear();
		Assert.IsTrue(Component.FindObjectsOfType<TestablePooledObject>().Length == 0);
		Assert.IsTrue(pool.NumAvailableInstances == poolCapacity * 2);
		Assert.IsTrue(pool.MaxInstances == poolCapacity * 2);
	}
}
